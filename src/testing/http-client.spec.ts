import { Task } from './../app/task';
import { mockOneTask } from './../app/mock-todos';
import { TestBed } from '@angular/core/testing';
import { HttpClient } from "@angular/common/http";
import { HttpClientTestingModule,HttpTestingController } from "@angular/common/http/testing";



describe('Testing HttpClient', ()=>{
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(()=>{
    TestBed.configureTestingModule({
      imports:[HttpClientTestingModule]
    });

    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(()=>{
    httpTestingController.verify();
  });

  it('Testing GET method', ()=>{
      httpClient.get<Task>('/todos/1').subscribe(task => expect(task).toEqual(mockOneTask));

      const req = httpTestingController.expectOne('/todos/1');
      expect(req.request.method).toEqual('GET');
      req.flush(mockOneTask);
  });


});
