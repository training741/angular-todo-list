import { ErrorHandler } from "@angular/core";

export class TodoErrorHandler implements ErrorHandler{
  handleError(error: any): void {
    throw new Error("Something goes wrong");
  }

}
