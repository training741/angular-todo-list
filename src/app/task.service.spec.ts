
import { TodoList } from './todoList';
import { ErrorHandler } from '@angular/core';
import { TODOS, mockOneTask } from './mock-todos';
import { of } from 'rxjs';
import { TestBed } from '@angular/core/testing';

import { TaskService } from './task.service';
import { waitForAsync } from '@angular/core/testing'
import { HttpClientTestingModule , HttpTestingController} from '@angular/common/http/testing'
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Task } from './task';
import { compilePipeFromMetadata } from '@angular/compiler';

const TASK_URL = 'http://localhost:8080/v1/todos';
describe('TodoService', () => {
  let taskService: TaskService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [
        TaskService
      ]
    });
    taskService = TestBed.inject(TaskService);
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(()=>{
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(taskService).toBeTruthy();
  });
  describe('#getTask',()=>{
    let expectedTask: Task;
    beforeEach(() => {
      expectedTask = mockOneTask;
    });

    it('should return expected task (called once', () =>{
      const id = 1;
      taskService.getTask(id).subscribe({
        next: task => expect(task)
        .withContext('shoult return expected task')
        .toEqual(expectedTask),
        error: fail,
      });
      const req = httpTestingController.expectOne(`${TASK_URL}/${id}`);
      expect(req.request.method).toEqual('GET');
      req.flush(expectedTask);
    })
  });


  /*describe('Get observable array of todo', () =>{
    let expectedTasks: TodoList;
    beforeEach(()=>{
      todoService = TestBed.inject(TaskService);
      //expectedTasks =  ;
    })
    it('#getTodos should return an observable array of todo',(done) => {

      //todoService.getTodos().subscribe(todos => expect(todos).toEqual(TODOS));
        todoService.getTasks()
        .subscribe({next : todos => expect(todos).toEqual(expectedTasks),
        error: fail});
        done();
    });

  });*/
});


