import { Task } from './task';
export interface TodoList{
  _embedded?:{
    todoList: Task[]
  };

  _links:{
    self: {
      href: string;
    }
  };

}
