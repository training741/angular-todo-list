import { MockTodoService } from './../../testing/mock-service';
import { Observable, of } from 'rxjs';
import { TaskService } from './../task.service';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TodosComponent } from './todos.component';
import { Task } from '../task';

//======
/*describe('TodoComponent',() => {
  let todoComponent: TodosComponent;
  let todoService: TodoService;

  beforeEach(()=> {
    TestBed.configureTestingModule({
      providers: [
        TodosComponent,
        {provide: TodoService, useClass: MockTodoService}
      ]
    });
    todoComponent = TestBed.inject(TodosComponent);
    todoService = TestBed.inject(TodoService);
  });


  it('should have empty list of todo before calling ngOnInit',()=>{
    expect(todoComponent.todos).toEqual([]);
  });

  it('should have a list of todo after calling ngOnInit',()=>{
    todoComponent.ngOnInit();
    expect(todoComponent.todos.length).toBe(2);
  });

});*/


/*describe('TodosComponent', () => {
  let component: TodosComponent;
  let fixture: ComponentFixture<TodosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TodosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TodosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});*/
