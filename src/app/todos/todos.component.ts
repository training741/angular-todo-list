import { TodoList } from './../todoList';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { TaskService } from './../task.service';
import { Component, OnInit } from '@angular/core';
import { Task } from '../task';
import { getCreationMessage } from '../utils/dateUtils';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  tasks?: TodoList;

  constructor(private taskService: TaskService) { }

  ngOnInit(): void {
    this.getTasks();
  }

  getTasks(): void{
    //let tasks: TodoList;
    this.taskService.getTasks().subscribe(tasks  => {
      tasks._embedded?.todoList.forEach((task)=>{
        task.creationMessage = this.getCreationMessage(task.createdAt);
      });
      this.tasks = tasks;

    });


  }

  addTodo(title: string,completed: boolean) {
    if(title.trim()===''){
      return;
    }
    this.taskService.addTask({title, completed} as Task).subscribe(
      task => {
        console.log(task);
        this.getTasks();
      });
  }

 displayFound(foundTasks: TodoList) {
    foundTasks._embedded?.todoList.forEach((task) =>{
      task.creationMessage = getCreationMessage(new Date(task.createdAt).getTime())
    });
    this.tasks =foundTasks;
  }

  getCreationMessage(date: string): string{
    return getCreationMessage(new Date(date).getTime());
}

}
