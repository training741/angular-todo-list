import { TodoList } from './../todoList';
import { TODOS } from './../mock-todos';
import { MockTodoService } from './../../testing/mock-service';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TaskService } from '../task.service';

import { TodoSearchComponent } from './todo-search.component';



/*describe('TodoSearchComponent', () => {
  let component: TodoSearchComponent;
  let fixture: ComponentFixture<TodoSearchComponent>;
  let mockTaskService : TaskService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TodoSearchComponent ],
      providers: [TodoSearchComponent,{provide:TaskService, useClass: MockTaskService}]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TodoSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockTaskService = TestBed.inject(TaskService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  describe("#searchTodosMatchingPattern() ", ()=>{
    it('Should emit TODOS when searchin with empty pattern', ()=>{
      spyOn(component.todosFound, 'emit');
      component.searchTodosMatchingPattern('');
      fixture.detectChanges();
      expect(component.todosFound.emit)
        .withContext("searching with empty string")
        .toHaveBeenCalledWith(TODOS);
    });

    it('Should emit an array of two object when searching with "Angular pattern"', ()=>{

      spyOn(component.todosFound, 'emit');
      component.searchTodosMatchingPattern('Angular');
      fixture.detectChanges()
      expect(component.todosFound.emit)
        .withContext("Search with Angular pattern")
        .toHaveBeenCalledWith([{id:7, description: "Angular my favorite for frontend", done: true},
        {id:6, description: "Master Angular", done: true}])
    });
  });

});*/
