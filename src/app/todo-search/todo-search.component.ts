import { TodoList } from './../todoList';
import { TaskService } from './../task.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Task } from '../task';
@Component({
  selector: 'app-todo-search',
  templateUrl: './todo-search.component.html',
  styleUrls: ['./todo-search.component.css']
})
export class TodoSearchComponent implements OnInit {

  @Output() todosFound = new EventEmitter<TodoList>();
  constructor(private taskService: TaskService) { }

  ngOnInit(): void {
  }

  searchTodosMatchingPattern(pattern: string){
      this.taskService.searchTasksMatchingPattern(pattern)
        .subscribe(tasks => this.todosFound.emit(tasks));
  }



}
