import { TaskService } from './../task.service';
import { Task } from './../task';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { getCreationMessage } from '../utils/dateUtils';

@Component({
  selector: 'app-todo-detail',
  templateUrl: './todo-detail.component.html',
  styleUrls: ['./todo-detail.component.css']
})
export class TodoDetailComponent implements OnInit {

  title: string = "Todo details"
  buttonValidationTitle: string = "Save"
  task?: Task; //={id:1,description:"test", active:true};
  constructor(
    private taskService: TaskService,
    private route: ActivatedRoute,
    private location: Location) { }

  ngOnInit(): void {
    this.getTodo();
  }

  getTodo():void{
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.taskService.getTask(id).subscribe(task=> this.task = task);

  }

  addOrUpdate() {
    console.log('task to update: ',this.task)
    if(!this.task || this.task.title.trim()===''){
      return;
    }
    this.taskService.updateTask(this.task).subscribe(task => {
      console.log('task updated: ',task);
      this.goBack();
    });

  }
  goBack() {
    this.location.back();
  }
}
