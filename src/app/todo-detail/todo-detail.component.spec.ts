import { MockTodoService, mockTodo } from './../../testing/mock-service';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { TaskService } from './../task.service';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoDetailComponent } from './todo-detail.component';

/*describe('TodoComponentDetail', ()=>{
  beforeEach(()=>{
    TestBed.configureTestingModule({
      providers: []
    })
  });
});*/

/*describe('TodoDetailComponent', () => {
  let component: TodoDetailComponent;
  let fixture: ComponentFixture<TodoDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TodoDetailComponent ],
      imports: [
        RouterTestingModule
      ],
      providers: [
        {provide: TodoService, useClass: MockTodoService}
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TodoDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should be equal to {id:300,description:'todo mock', done: true}", () =>{
    expect(component.todo).toBe(mockTodo);
  });
});*/
