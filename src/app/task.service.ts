import { TodoList } from './todoList';

import { Injectable } from '@angular/core';
import {  Observable, of, throwError } from 'rxjs';
import { Task } from './task';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError} from 'rxjs/operators';


const TASK_URL = 'http://localhost:8080/v1/todos';
const TASK_SEARCH_URL = 'http://localhost:8080/v1/task_search';


@Injectable({
  providedIn: 'root'
})
export class TaskService {
   hearders ={ headers: new HttpHeaders({
    'Content-Type': 'application/json'})};
  constructor( private httpClient: HttpClient ) { }

  getTask(id:number): Observable<Task>{
    return this.httpClient.get<Task>(TASK_URL+`/${id}`).pipe(
      catchError(this.handleErrorAccordingStatus)
    );
  }


  getTasks():Observable<TodoList>{
    return this.httpClient.get<TodoList>(TASK_URL).pipe(
      catchError(this.handleErrorAccordingStatus)
    );
  }
  handleError<T>(operation= 'opration', result?: T){
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    }
  }
  private handleErrorAccordingStatus(error: HttpErrorResponse){
    if(error.status === 0){
      console.error('An error occured:', error.error);
    }else{
      console.error(`Backend returned code ${error.status}, body was`, error.error);
    }

    return throwError(() => new Error('Something bad happened; please try again later.'));
  }
  updateTask(Task: Task):Observable<Task> {
    return this.httpClient.put<Task>(Task._links.self.href,Task,this.hearders).pipe(
      catchError(this.handleErrorAccordingStatus)
    );
  }
  addTask(Task: Task): Observable<Task> {
   return this.httpClient.post<Task>(TASK_URL,Task,this.hearders).pipe(
      catchError(this.handleErrorAccordingStatus)
    )
  }

  searchTasksMatchingPattern(term: string): Observable<TodoList> {
    let urlParams = new HttpParams({fromString: `term=${term}`});
    let options = {params: urlParams};
    return this.httpClient.get<TodoList>(TASK_SEARCH_URL, options).pipe(
      catchError(this.handleErrorAccordingStatus)
    );

  }
}
