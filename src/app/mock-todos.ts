import { Task, Links } from "./task";

const links ={ self:{href:'link'}, todos:{href:'link'} } as Links;
export const mockOneTask: Task = {id:1,title:"Dsiplay todos", completed: true, createdAt: '2022-11-23T12:39:24.309885', creationMessage: 'Created today',_links: links }
export const TODOS: Task[] = [
  {id:4, title: "establish CEMU card", completed: true, createdAt: '2022-11-23T12:39:24.309885', creationMessage: 'Created today',_links: links },
  {id:5, title: "licence driving", completed: false, createdAt: '2022-11-23T12:39:24.309885', creationMessage: 'Created today',_links: links },
  {id:6, title: "Finish Java training", completed: true, createdAt: '2022-11-23T12:39:24.309885', creationMessage: 'Created today',_links: links },
  {id:7, title: "Angular my favorite for frontend", completed: true, createdAt: '2022-11-23T12:39:24.309885', creationMessage: 'Created today',_links: links },
  {id:6, title: "Master Angular", completed: true, createdAt: '2022-11-23T12:39:24.309885', creationMessage: 'Created today',_links: links }
]
