export interface Links  {
  self:{
    href:string;
  };
  todos:{
    href: string;
  }
}
export interface Task{
  id: number;
  title: string;
  completed: boolean;
  createdAt: string;
  creationMessage: string;

  _links: Links

}
