const TODAY = '0';
const YESTERDAY='1';
const ONE_DAY_IN_MILLI = 1000 * 60 * 60 *24;
export function calculateNumberOfDaysSinceCreationDate(creationDate: number):string{
  let dateDiff = Date.now() - creationDate;
  let days = Math.round(dateDiff/ONE_DAY_IN_MILLI);
  return days.toFixed(0);
}

export function getCreationMessage(creationDate: number){
   let days = calculateNumberOfDaysSinceCreationDate(creationDate);
  if(days === TODAY){
    return 'Created today';
  }

  if(days === YESTERDAY){
    return "Created yesterday";
  }
  return `Created ${days} days ago`;

}
