import { generateNumber, MAX, MIN } from "./generator";

describe('generator',()=>{
  it('should return a number greater than or equal to 10',()=>{
    expect(generateNumber()).toBeGreaterThanOrEqual(MIN);
  });
  it('should return a number less than or equal to 997997', ()=>{
    expect(generateNumber()).toBeLessThanOrEqual(MAX);
  });
});
