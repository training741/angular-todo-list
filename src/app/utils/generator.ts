export const MIN : number = 10;
export const MAX: number  = 997997;

export function generateNumber(){
   let difference = MAX - MIN;
   let randomNumber = Math.random();
   randomNumber = Math.floor( randomNumber * difference);
   randomNumber = randomNumber + MIN;
   return randomNumber;
}

